import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_tutorial/model/monster.dart';
//import 'package:hive_tutorial/model/monster.g.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Hive database Demo")),
      body: FutureBuilder(
        future: Hive.openBox("monsters"),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError)
              return Center(
                child: Text(snapshot.error),
              );
            else {
              var monsters = Hive.box("monsters");
              if (monsters.length == 0) {
                monsters.add(Monster("Vampire", 1));
                monsters.add(Monster("Jelly guardian", 5));
              }
              return ListView.builder(
                itemCount: monsters.length,
                itemBuilder: (context, index) {
                  Monster monster = monsters.getAt(index);
                  return Text(
                      monster.name + " [" + monster.level.toString() + "]");
                },
              );
            }
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }
}
